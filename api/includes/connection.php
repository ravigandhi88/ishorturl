<?php

	class bt_DB extends My_DB {			
		function __construct() {
			register_shutdown_function( array( $this, '__destruct' ) );
			
			$this->dbuser = DB_USER;
			$this->dbpassword = DB_PASSWORD;
			$this->dbname = DB_NAME;
			$this->dbhost = DB_HOST;
	
			$this->db_connect();
		}
	}

	// $db = new bt_DB();

?>