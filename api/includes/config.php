<?php
	class config {

		private static $options = array(

			"max_length" => array(
					'domain' => '253',
					'url' => '1500',
					'title' => '200',
					'note' => '250'
				),

			// Used to determine what the user is requesting
			"query_string" => array(
					'user' => 'user',
					'domain' => 'domain',
					'link' => 'link',
					'visit' => 'visit',
					'log' => 'log'
				),

			"action_query_string" => "action",

			"xml_root_name" => "root",

			"short_link_length" => 6,

			"response_content_type" => "json"
			
		);
		
		public static function get( $key = false ){
			return $key !== false ? self::$options[$key] : self::$options;
		}

	}
?>