<?php

	class restful_api {

		// Type of page content
		protected $content_type = "json";

		// Content Mime-Type
		protected $mime_type = "application/json";

		// request method e.g. PUT, POST, GET, DELETE
		protected $request_method = null;

		// Data request sent by user
		protected $request_data = null;

		// HTTP response code
		protected $http_response_code = null;

		/**
		 * Set+Get request method name. Works as setter and getter
		 * If method name is not provided then it will automatically request method from server variable
		 *
		 * @param string $method method name
		 * @return string request method name
		 * @access public
		 */
		public function method ( $method = null ) {
			// If $method is provided then set method
			if ( $method ) $this->request_method = $method;
			// If still method is not set then set method from $_SERVER varialbe
			if ( !$this->request_method ) $this->request_method = $_SERVER['REQUEST_METHOD'];
			return $this->request_method;
		}

		/**
		 * Check if the method is a valid method
		 * Valid methods are those in $request_methods array
		 *
		 * @param string $method method name
		 * @return boolean
		 * @access public
		 */
		public function is_valid_method ( $method = null ) {
			if ( !$method ) $method = $this->request_method;
			global $request_methods;
			return in_array( strtoupper($method), $request_methods);
		}

		/**
		 * Return HTTP Response Code
		 * It returns the numeric and code detail
		 *
		 * @param numeric $code HTTP Response code
		 * @return string http-response-code and descriotion
		 * @access public
		 */
		public function return_http_response_code ( $code = null ) {
			global $http_status_codes;
			return $code . " " . $http_status_codes[$code];
		}

		/**
		 * Set+Get Set HTTP Response Code. Works as setter and getter
		 * It returns the numeric value for code
		 *
		 * @param numeric $code HTTP Response code
		 * @return numeric http-response-code
		 * @access public
		 */
		public function http_response_code ( $code = null ) {
			if ( !$code ) return $this->http_response_code;
			// set http_response_code
			http_response_code( $code );
			// assign code value to http_response_code variable in this class
			$this->http_response_code = $code;
			return $this->http_response_code;
		}

		/**
		 * Set page content type
		 *
		 * @param string $type Page content type extension
		 * @return string mime-type
		 * @access public
		 */
		public function content_type ( $type = null ) {

			if ( !$type ) return $this->content_type;

			if ( $type ) $this->content_type = $type;

			$this->mime_type = $this->get_mime_type($this->content_type);

			header('Content-type: ' . $this->mime_type );

			return $this->mime_type;

		}

		/**
		 * Get mime type from file extension
		 *
		 * @param string $extension file extension
		 * @return string mime-type/null
		 * @access public
		 */
		public function get_mime_type ( $extension = null ) {

			if ( !$extension ) return $this->mime_type;

			global $mime_types;

			$extension = strtolower($extension);

			if ( $extension && array_key_exists($extension, $mime_types) ) {
				return $mime_types[$extension];
			}

			return null;

		}

		/**
		 * Get data for each type of request method
		 *
		 * @param string $method name of request method
		 * @return object/null data as object or null value
		 * @access public
		 */
		public function get_request_data ( $method = null ) {

			if ( $this->request_data ) return $this->request_data;

			if ( !$method ) $method = $this->request_method;

			// change method name to uppercase
			$method = strtoupper($method);

			switch ( $method ) {
				case 'POST':
					$request_data =$_POST;
					break;
				case 'GET':
					$request_data =$_GET;
					break;
				case 'PUT':
					$request_data = $this->_parse_string( file_get_contents("php://input") );
					break;
				case 'DELETE':
					$request_data = $this->_parse_string( file_get_contents("php://input") );
					break;
				case 'HEAD':
					$request_data = file_get_contents("php://input");
					break;
				case 'OPTIONS':
					$request_data = file_get_contents("php://input");
					break;
				default:
					$request_data = null;
					break;
			}

			$this->request_data = $request_data;

			return $this->request_data;
		}

		/**
		 * Parse string to object
		 *
		 * @param string $query input value query string
		 * @return object object converted from query string
		 * @access protected
		 */
		protected function _parse_string ( $query = null ) {

			$_delete_args = null;

			if ( !empty( $query ) ) {
				foreach( explode('&', $query ) as $param ) {
					list($k, $v) = explode('=', $param);
					$k = urldecode($k);
					$v = urldecode($v);
					if ( isset( $_delete_args[$k] ) ) {
						if ( is_scalar( $_delete_args[$k] ) ) {
							$_delete_args[$k] = array( $_delete_args[$k] );
						}
						$_delete_args[$k][] = $v ;
					} else {
						$_delete_args[$k] = $v;
					}
				}
			}

			return $_delete_args;

		}

		/**
		 * Return response in defined format
		 *
		 * @param array $data PHP data array
		 * @param string $format response type
		 * @return string JSON/XML/etc..
		 * @access public
		 */
		public function return_response ( $data, $format = null ) {

			if ( !$format ) $format = $this->content_type;

			$format = strtolower($format);
			
			switch ($format) {

				case 'json':
					return $this->get_json( $data );
					break;

				case 'xml':
					return $this->get_xml( $data );
					break;

				default:
					return $this->get_json( $data );
					break;
			}

		}

		/**
		 * Get JSON String
		 *
		 * @param array $data PHP data array
		 * @return string JSON String
		 * @access private
		 */
		private function get_json ( $data ) {
			return json_encode($data, JSON_PRETTY_PRINT);
		}

		/**
		 * Get XML String
		 *
		 * @param array $data PHP data array
		 * @return string XML String
		 * @access private
		 */
		private function get_xml ( $data ) {
			return Array2XML::createXML( config::get("xml_root_name") , $data)->saveXML();
		}

	}

?>