<?php
	/*

		Pending:
		- get_domain_id
		- get_owner_id
		- create_short_link
		- check_short_link_available

	*/

	class app {

		/**
		 * Get ID of the Domain
		 *
		 * @param string $domain: domain name
		 * @return number: id of domain
		 * @access public
		 */
		public static function get_domain_id ( $domain = false ) {
			return $domain ? 1 : 0;
		}

		/**
		 * Get ID of the Domain owner
		 *
		 * @param string $domain: domain name
		 * @return number: id of domain owner
		 * @access public
		 */
		public static function get_owner_id ( $domain = false ) {
			return $domain ? 1 : 0;
		}


		/**
		 * Create unique short link
		 * Generate a unique short link which doesn't exist in database
		 *
		 * @param string $url: URL of the webpage to be shortened
		 * @param string $domain: domain under which the link will be shortened
		 * @return string: short link
		 * @access public
		 */
		public static function create_short_link ( $url, $domain = DEFAULT_DOMAIN ) {

			return self::generate_short_link( config::get("short_link_length") );

		}
		/**
		 * Generate random string
		 * standalone function to generate random string of a certain length
		 *
		 * @param number $length: total number of character for the random string to be generated
		 * @return string: short link with specified length
		 * @access public
		 */
		public static function generate_short_link ( $length = 10 ) {
			
			$numbers = range(0, 9);
			$smallLetters = range('a', 'z');
			$capitalLetters = range('A', 'Z');
			$specialChars = array("_");

			$characters = join( "", array_merge($numbers, $smallLetters, $capitalLetters, $specialChars) );
			$charactersLength = strlen($characters);
			
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}

			return $randomString;
		}
		/**
		 * Check if a short link is available
		 * standalone function to check if a short link is available
		 *
		 * @param string $short_link: short link which needs to be checked for availability
		 * @param string $domain: domain under which the link exists
		 * @return boolen: true if the link is available and false if the link already exists for specified $short_link and $domain
		 * @access public
		 */
		public static function check_short_link_available ( $short_link, $domain ) {

		}


		/*
		 * Get details about a web page
		 * e.g.
		 * - Page Title
		 * - Meta tags
		 * - Link tags
		 */
		public static function get_page_title ( $url ) {
			$title = self::fetch_page_title( $url );
			return $title ? $title : $url;
		}
		public static function fetch_page_head_details ( $url ) {

			$return = array();

			$doc = new DOMDocument();
			@$doc->loadHTMLFile( $url );
			$xpath = new DOMXPath( $doc );

			// Get Meta Data
			$meta = array();
			foreach ($xpath->query('/html/head/meta') as $val) {
				if ($val->getAttribute('name')) {
					$meta[$val->getAttribute('name')] = $val->getAttribute('content');
				} else if ($val->getAttribute('property')) {
					$meta[$val->getAttribute('property')] = $val->getAttribute('content');
				}
			}
			if ( $meta ) $return['meta'] = $meta;

			// Get Link Data
			$link = array();
			foreach ($xpath->query('/html/head/link') as $val) {
				if ($val->getAttribute('rel')) {
					$key = ( $val->getAttribute('sizes') ) ? $val->getAttribute('rel') ."-". $val->getAttribute('sizes') : $val->getAttribute('rel');
					$link[$key] = $val->getAttribute('href');
				}
			}
			if ( $link ) $return['link'] = $link;

			// Get Title
			$title = $xpath->query('//title')->item(0)->nodeValue;
			if ( $title ) $return['title'] = $title;

			// Set return to false if there is no data
			if ( !$return ) $return = false;
			
			return $return;

		}
		public static function fetch_page_title ( $url ) {

			$doc = new DOMDocument();
			@$doc->loadHTMLFile( $url );
			$xpath = new DOMXPath( $doc );

			return $xpath->query('//title')->item(0)->nodeValue;

		}
		public static function fetch_meta_tag_details ( $url ) {

			$url = parse_url( $url ) ;

			$tags = get_meta_tags( $url['scheme'].'://'.$url['host'].$url['path'] );
			
			return $tags;
		}

	}
?>