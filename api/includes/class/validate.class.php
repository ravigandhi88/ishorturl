<?php
	################################################################################
	## Version: 1.0.0
	## Version: 1.1.0
	##	- 	Change the logic, if the rules are not set then use default rules. If not
	##		use the combination of default rule and the rules set by the user.
	################################################################################
	class validate {
		
		/**
		 * Replacable variables: value, val, label
		 */
		
		var $_errors;
		var $_data;
		var $_curElement;
		var $_curRule;
		var $_curErrMsgs;
		var $_get_asso_array = false;
		/**
		 * Determine how many errors messages will be visible
		 * true: show all errors message
		 * false: show only first error message
		 */
		var $_show_all_errors = false;
		/**
		 * Determines if all fields are valid
		 * value will turn false if any of the field fails validation
		 */
		protected $valid = true;
		
		var $_fields = array();
		/*********************************************
		/* FORMAT OF FIELDS
		**********************************************
		var $_fields = array(
				array(
					'name' => '',
					'input_type' => '',
					'label' => '',
					'value' => '',		// Accept: single value, array
					'has_unique_value' => '',	// Accept: single value, array
					'validate' => array(
						'err_msg' => array(		// Accept: single string, associative array
							'require' => '',
							'unique_value' => '',
							'number' => '',
							'int' => '',
							'float' => '',
							'ipv4' => '',
							'ipv6' => '',
							'bool' => '',
							'date' => '',
							'email' => '',
							'url' => '',
							'domain' => '',
							'json' => '',
							'depends' => array(
									'depend_on' => '',
									'custom_validate' => '',
								),
							'compare' => array(
									'compare_to' => '',
									'is_equal' => '',
									'is_not_equal' => '',
									'is_greater' => '',
									'is_greater_equal' => '',
									'is_less' => '',
									'is_less_equal' => '',
									'custom_validate' => '',
								),
							'password' => array(
											'digits' => '',
											'capital_letters' => '',
											'small_letters' => '',
										),
							'pattern' => '',
							'max_length' => '',
							'min_length' => '',
							'eq_length' => '',
							'is_less' => '',
							'is_greater' => '',
							'is_not_equal' => '',
							'custom_validate' => '',
						),
						'trim' => true,			// Accept: true
						'case_sensitive' => '',	// Accept: true
						'rules' => array(		// Accept: single string, associative array
							'require' => true,		// Accept: true
							'unique_value' => array(
								'table' => '', // Name of the table
								'field' => '',	// Field of the table
								'primary_field' => '',	// Primary key field name
								'primary_value' => '',	// Primary key field value (if will not compare with current field value)
							),
							'number' => '',			// Accept: true
							'int' => '',			// Accept: true
							'float' => '',			// Accept: true
							'ipv4' => '',			// Accept: true
							'ipv6' => '',			// Accept: true
							'bool' => '',			// Accept: true
							'date' => '',			// Accept: true, array like array('format','d/m/Y')
							'email' => '',			// Accept: true
							'url' => '',			// Accept: true
							'domain' => '',			// Accept: true
							'json' => '',			// Accept: true
							'depends' => array(	// Check only if 'depend_on' field value is not empty. Supports multiple validation on depend.
									'depend_on' => 'checkthis',
									'custom_validate' => '',	// Accept: single function, array of functions
								),
							'compare' => array(	// Compare with a field
									'compare_to' => 'name1',
									'is_equal' => true,			// Accept: True
									'is_not_equal' => true,		// Accept: True
									'is_greater' => true,		// Accept: True
									'is_greater_equal' => true,	// Accept: True
									'is_less' => true,			// Accept: True
									'is_less_equal' => true,	// Accept: True
									'custom_validate' => '',	// Accept: single function, array of functions
								),
							'password' => array(
											'digits' => '',			// Accept: True
											'capital_letters' => '',// Accept: True
											'small_letters' => '',	// Accept: True
										),
							'pattern' => '',			// Accept: single pattern, array of patterns, Invalid: when none of the patterm matches with the value
							'max_length' => '',			// Accept: integer
							'min_length' => '',			// Accept: integer
							'eq_length' => '',			// Accept: integer, array of integers, Invalid: when length matches none of the values in eg_length
							'equal_to' => '',			// Accept: single string, array of strings
							'is_not_equal' => '',		// Accept: single string, array of strings
							'is_less' => '',			// Accept: float
							'is_less_equal' => '',		// Accept: float
							'is_greater' => '',			// Accept: float
							'is_greater_equal' => '',	// Accept: float
							'must_contain' => '',		// Accept: single string, array of strings, Case-sensitive
							'must_contain_i' => '',		// Accept: single string, array of strings, Case-insensitive
							'must_not_contain' => '',	// Accept: single string, array of strings, Case-sensitive
							'must_not_contain_i' => '',	// Accept: single string, array of strings, Case-insensitive
							'custom_validate' => '',	// Accept: single function, array of functions
						),
					),
				),
			);
		*/
		
		/**
		 * Default Arguments
		 * These data will be used when any of the necessary arguments are not passed to the function
		 * default_date_format and default_datetime_format both supports array.
		 *		If the value matches any of the array value then it will be considered as Valid.
		 */
		var $_args = array(
			'db_args' => array(
					'db' => '',
					'sql' => '',
					'table_name' => '',
					'primary_field_name' => '',
					'primary_field_value' => '',
				),
			'default_date_format' => array('d/m/y', 'd/m/Y', 'd-m-y', 'd-m-Y'),
			'default_datetime_format' => array('d/m/Y H:i:s', 'd/m/y H:i:s', 'd-m-Y H:i:s', 'd-m-y H:i:s'),
		);
		
		var $default_err_msgs = array(
				'require' => '{{label}} must not be empty!',
				'unique_value' => '{{label}} must be unique!',
				'number' => '{{label}} must be a Number!',
				'int' => '{{label}} must be an Integer!',
				'float' => '{{label}} must be an Float!',
				'ipv4' => '{{label}} must be a valid IPv4',
				'ipv6' => '{{label}} must be a valid IPv6',
				'bool' => '{{label}} must be a valid Boolean',
				'date' => '{{label}} must be a valid date!',
				'email' => '{{label}} must be a valid Email Address!',
				'url' => '{{label}} must be a valid URL!',
				'domain' => '{{label}} must be a valid domain!',
				'json' => '{{label}} must be a valid JSON!',
				'password' => '{{label}} must be a valid Password!',
				'pattern' => '{{label}} must match the Pattern!',
				
				'depends' => 'Depend validation failed!',
				'compare' => 'Comparison failed!',

				'max_length' => '{{label}} must not be more than {{val}} characters!',
				'min_length' => '{{label}} must not be less than {{val}} characters!',
				'eq_length' => '{{label}} must contain {{val}} characters!',
				
				'is_equal' => '{{label}} must be equal to {{val}}!',
				'is_not_equal' => '{{label}} must not be equal to {{val}}!',
				'is_less' => '{{label}} must be less than {{val}}!',
				'is_less_equal' => '{{label}} must be less than or equal {{val}}!',
				'is_greater' => '{{label}} must be greater than {{val}}!',
				'is_greater_equal' => '{{label}} must be greater than or equal to {{val}}!',
				
				'must_contain' => '{{label}} must contain following string: {{val}}',
				'must_contain_i' => '{{label}} must contain following string: {{val}}',
				'must_not_contain' => '{{label}} must not contain following string: {{val}}',
				'must_not_contain_i' => '{{label}} must not contain following string: {{val}}',
				
				'custom_validate' => 'Custom Validation Failed',
			);
		
		/**
		 * Default rules to be set for the fields for which there are no rules set
		 * 		for particular input_type
		 * e.g. If input_type is date and there is no rule set for this field then
		 *		automatically add new rule 'date'=>true inside rules
		 */
		var $_default_data_type_rules = array (
				'date' => 'date',
				'datetime' => 'datetime',
				'email' => 'email',
				'url' => 'url',
				'domain' => 'domain',
				'number' => 'number',
			);

		/**
		 * Default Rules are used when rule values are not set for particular rule
		 * e.g. even if max_length value is not set, the function will apply value of max_length value from _default_rules
		 */
		private $_default_rules = array(
					'max_length'=>'100',
				);
		public function __construct($args = array()) {

			$vars_allowed_to_be_set = array(
										'_fields',
										'_args',
										'_data',
										'default_err_msgs',
										'_default_data_type_rules',
									);
			
			// This will do like:
			// $this->_args = array_merge($this->_args, $args);
			foreach ( $args as $key => $value ) {
				if ( in_array( $key, $vars_allowed_to_be_set ) ) {
					$this->$key = array_merge( (array)$this->$key, $value );
				}
			}
		}
		
		/***********************************************************
		*
		*	Primary Validation Methods
		*
		***********************************************************/
		
		// Set Data
		public function setData($data) {
			$this->_data = $data;
		}
		
		// Validate multiple fields
		public function doMassValidation () {
			$isValid = true;
			foreach ( $this->_fields as $field ) {
				if ( !$this->doSingleValidation($field) ) {
					$isValid = false;
					$this->valid = false;
				}
			}
			return $isValid;
		}
		// Do validation of single field
		public function doSingleValidation ($field) {
			
			// Set current element
			$this->setCurrentElement($field);
			
			/*
			 * Set Default Rules for particular input types
			 * e.g. for date input type automatically add rule for date validation if date doesn't exist inside rules
			 */
			$field = $this->addRelevantDefaultRules($field);
			
			// If Validate and Rules are not set in the field arguments
			if ( !isset($field['validate']) || !isset($field['validate']['rules'])) {
				/**
				 * If rules are not set then just use the default rules
				 */
				$rules = $this->_default_rules;
			} else {
				/**
				 * If rules are set then check if the rules are array. If rules are not array then convert it to array.
				 * (Ccnverting to array will save code of validating as STRING ONLY. Just Loop through array value and validate.)
				 */
				$rules = ( !is_array($field['validate']['rules']) ? array( $field['validate']['rules'] => true ) : $field['validate']['rules'] );
			
				// Set default with $rules
				$rules = array_merge($this->_default_rules, $rules);
			}
			
			 // Get Value of the field. If value exists then set the value or set value to false
			$value = ( isset($this->_data[$field['name']]) ? $this->_data[$field['name']] : false );

			// Check if the field is required and not empty
			// If the field is not required and empty then return and do not do validation
			if ( !isset($rules['require']) || ($rules['require'] !== true) ) {
				// Check if the value is empty
				if (!$this->isRequired($value) && (!isset($rules['depends']))) {
					return true;
				}
			}
			
			/**
			 * If Error Messages exists then get the error messages.
			 * If error messages are array then merge them with Default Error Messages else set current error message to the single error message string
			 */
			$err_msgs = ( isset($field['validate']['err_msg']) ? $field['validate']['err_msg'] : array() );
			$this->_curErrMsgs = is_array($err_msgs) ? array_merge($this->default_err_msgs, $err_msgs) : $err_msgs;
			
			return $this->processRules ($rules, $value, true, false);
		}
		
		// Process rules and return true/false accordingly
		// Set necessary error messages
		public function processRules ($rules, $value, $setCurrentRule=true, $hasSubErrMsgs = false, $extraStrReplace = array()) {
			// Set rules processed counter
			static $counter = 0; $counter++;
			
			// Initiate $isValid and $curr_err variables
			$isValid = true;
			$curr_err = array();
			
			// Loop through each rule in the $rules array and validate
			foreach ( $rules as $rule => $valid_para ) {
				// Set Current Rule
				if ( $setCurrentRule === true ) { $this->_curRule = $rule; }
				
				// Validate each rule and do more processes if validation returns false
				$validate_result = $this->validateRule($rule, $value, $valid_para);
				
				if ( !$validate_result ) {	// Validation failed as $value didn't meet $rule requirements
					// set validation to false
					$isValid = false;
					
					// Get error message
					$subErrMsg = (($hasSubErrMsgs === true) ? $rule : false);
					$error = $this->getErrMsg($this->_curRule, $this->_curErrMsgs, $subErrMsg);
					
					// Do String replacement on $error
					$strReplacePara = array(
								'value'=>$value,
								'val'=>$valid_para,
								'label'=>$this->_curElement['label'],
							);
					$strReplacePara = array_merge($strReplacePara, $extraStrReplace);
					$error = $this->applyStrReplace($error, $strReplacePara);
					
					// Assign error to Field
					$curr_err[$this->_curRule] = $error;
					
				} elseif ( $validate_result === -1 ) {	// Validation Failed as validation function for $rule is not set
					$isValid = false;
					// Get error message
					$error = 'Fatal Error: Following rule has problem: "' . $this->_curRule . '". <br />- Either the rule has not been set<br />- Rule value is not valid';
					// Assign error to Field
					$curr_err[$this->_curRule] = $error;
				} else {
					// Extra Process (if any)
				}
				
				/**
				 * If validation fails and the rules are being checked for secold level validation 
				 * (e.g. compare, depends, password) then do not check for the next rule in second level validations
				 */
				if ( !$isValid && !$this->_show_all_errors ) {break;}
			}
			
			// Set $curr_err array format
			$this->setCurrentElementErrMsg($curr_err);

			return $isValid;
		}

		// Validate rule
		public function validateRule ($rule, $value, $para=true) {
			
			// If the $para is empty then return true and do not validate
			if ( empty($para) ) { return true; }
			
			// Data Types Array
			$types_array = array('require','number','int','float','bool','ipv4','ipv6','date','email','url','domain','json',);
			// If the data type is in $types_array and $para is not set to true (for all of the above array items) and $para is not array (for date) then do not validate
			if ( in_array($rule, $types_array) && ($para !== true) && (!is_array($para)) ){
				return true;
			}
			
			// Length Array
			$length_array = array('max_length','min_length','eq_length');
			// If the length is in $length_array and $para is not numeric then do not validate
			if ( in_array($rule, $length_array) ){
				// Check if the $para is array
				if ( is_array($para) ) {
					// Array is only allowed for eq_length
					// If $rule is not eq_length then return and do not validate
					if ( $rule != 'eq_length' ) {return true;}
					// Check all values of $para and remove non-numeric values
					foreach ($para as $key => $par) {
						if (is_numeric($par)) {
							$para[$key] = $par;
						} else {
							unset($para[$key]);
						}
					}
					// If after removing non-numeric value, there are no values left
					// Return true and do not validate
					if ( count($para) == 0 ) { return true; }
				} else {
					// If $para is not numeric then return true and do not validate
					if ( is_numeric($para) !== true ) { return true; }
				}
			}
			
			/* Check for each rule and validate
			 * If the rule doesn't match any of the below then see if any function exists with same name
			 * and try to validate using that function
			 */
			switch(strtolower($rule))
			{
				case "require"				:	return $this->isRequired($value); break;
				case "unique_value"			:	return $this->isUnique($value, $para); break;
				case "number"				:	return $this->isNumber($value); break;
				case "int"					:	return $this->isInt($value); break;
				case "float"				:	return $this->isFloat($value); break;
				case "bool"					:	return $this->isBool($value); break;
				case "ipv4"					:	return $this->isIpv4($value); break;
				case "ipv6"					:	return $this->isIpv6($value); break;
				case "date"					:	return $this->isDateTime($value, $para); break;
				case "datetime"				:	return $this->isDateTime($value, $para, true); break;
				case "email"        		:	return $this->isEmail($value); break;
				case "url"					:	return $this->isUrl($value); break;
				case "domain"				:	return $this->isDomain($value); break;
				case "json"					:	return $this->isJson($value); break;
				case "password"				:	return $this->isValidPassword($value, $rule, $para); break;
				case "pattern"				:	return $this->matchPattern($value, $para); break;
				
				case "max_length"			:	return $this->maxLength($value,$para); break;
				case "min_length"			:	return $this->minLength($value,$para); break;
				case "eq_length"			:	return $this->equalLength($value,$para); break;
				
				case "is_equal"				:	return $this->isEqualTo($value,$para); break;
				case "is_not_equal"			:	return $this->isNotEqualTo($value,$para); break;
				case "is_less"				:	return $this->isLessThan($value,$para); break;
				case "is_less_equal"		:	return $this->isLessThanEqual($value,$para); break;
				case "is_greater"			:	return $this->isGreaterThan($value,$para); break;
				case "is_greater_equal"		:	return $this->isGreaterThanEqual($value,$para); break;
				
				case "must_contain"			:	return $this->mustContain($value,$para, true); break;
				case "must_contain_i"		:	return $this->mustContain($value,$para, false); break;
				case "must_not_contain"		:	return $this->mustNotContain($value,$para, true); break;
				case "must_not_contain_i"	:	return $this->mustNotContain($value,$para, false); break;
				
				case "custom_validate"		:	return $this->validateCustomFunction($value, $para); break;
				default:
					/**
					 * $value is the value we get from form
					 * $rule will be the function name here
					 * $para will be extra parameters
					 * e.g. 'date' => array ('format' => 'ddmmyyyy'). $rule: date, $para: date array value
					 */ 
					return $this->validateCustomFunction($value, $rule, $para);
			}
			
			// If none of the above works then return true	
			return true;
		}
		// Reset all values so the object can be used for another new validation
		public function resetAll () {
			$this->_errors = 0;
		}
		// Set Default Values
		public function setDefaultValues () {
		}
		// Add relevant default rule to field
		public function addRelevantDefaultRules ( $field ) {
			// Check if input type exists in default data type rules
			if ( in_array( $field['input_type'], $this->_default_data_type_rules ) ) {
				// Check if the rule exists for this input type
				if ( !isset($field['validate']['rules'][$field['input_type']]) ) {
					// Set the rule for the field
					$field['validate']['rules'][$field['input_type']] = true;
				}
			}
			// Return the updated field
			return $field;
		}
		// Set current element
		public function setCurrentElement( $field ) {
			$this->_curElement = $field;
		}
		/**
		 * Get Error Message
		 * $rule: Rule for which you want to get error message
		 * $err_msgs: Arrays with all error message for current element
		 * $rule_element: Get a particular error message if error found is an array
		 */
		public function getErrMsg ($rule, $err_msgs, $rule_element = false) {
			if ( is_array($err_msgs) && isset($err_msgs[$rule]) ) {
				$error = $err_msgs[$rule];
			} elseif ( !is_array($err_msgs) && isset($err_msgs) ) {
				$error = $err_msgs;
			/*} elseif ( isset($default_err_msgs[$rule]) ) {
				$error = $default_err_msgs[$rule];*/
			} else {
				$error = 'Invalid value!';
			}
			if ( is_array($error) && ($rule_element !== false) && isset($error[$rule_element])) {
				$error = $error[$rule_element];
			}
			return $error;
		}
		/**
		 * Set Error Message
		 * - If current element error message exists, it will add new error message using smartMergeArray
		 *	 Else it will create current element error message
		 */
		public function setCurrentElementErrMsg ($err_msg) {
			// If there are no errors inside $err_msg then do not set error for this element and return
			if ( empty($err_msg) ) { return true; }
			
			// Set $curr_err array format
			$err_msg = (($this->_get_asso_array !== true) ? $err_msg : array_values($err_msg));
			
			// Get current element field
			$field = $this->_curElement;
			
			// Add the value to _errors[name] array
			if ( !isset($this->_errors[$field['name']]) ) {
				$this->_errors[$field['name']] = $err_msg;
			} else {
				// Add $err_msg array to current element error without overwriting existing error messages
				$this->_errors[$field['name']] = $this->smartMergeArrays($this->_errors[$field['name']], $err_msg);
			}
			
			return $this->_errors[$field['name']];
		}
		/**
		 * Replace the {{val}} to necessary $value parameters
		 * - Searches value of each {$key} inside the string and changes it with $value
		 */
		public function applyStrReplace ($str, $replacement_array) {
			if (is_array($str)) return;
			$replace_array = $replacement_array;
			foreach ( $replace_array as $temp_key => $temp_value ) {
				$temp_value = is_array($temp_value) ? serialize($temp_value) : $temp_value;
				$replace_array['{{'.$temp_key.'}}'] = $temp_value;
				unset($replace_array[$temp_key]);
			}
			// Replace all occurance of any string matching key of $replace_array with value of $replace_array
			$str = strtr($str, $replace_array);
			return $str;
		}
		/**
		 * Smarter way of merging arrays
		 * - Checks each value of $array2 and adds it to $array1 if it doesn't exist in $array1
		 * - Use this function when you want to add value of $array2 to $array1
		 *	 without changing any element of $array1
		 */
		public function smartMergeArrays ( $array1, $array2 ) {
			foreach ( $array2 as $key => $value ) {
				if ( !isset($array1[$key]) ) {
					$array1[$key] = $value;
				}
			}
			return $array1;
		}

		/***********************************************************
		*
		*	Different Types of Validation functions
		*
		***********************************************************/
		
		// Check if the field value is Empty
		public function isRequired ($value) {
			return is_array($value) ? !(count($value)==0) : !(trim($value)=="");
		}
		// Check if he field value is unique in the database
		public function isUnique ($value, $para) {
			// Get database arguemnts
			$db_args = $this->_args['db_args'];
			// Get database
			$db = $db_args['db'];
			
			$para = (array)$para;
			// Get field name, table name, primary field name and primary field value
			$field = isset($para['field']) ? $para['field'] : $this->_curElement['name'];
			$table = isset($para['table']) ? $para['table'] : $db_args['table_name'];
			$primary_field = isset($para['primary_field']) ? $para['primary_field'] : $db_args['primary_field_name'];
			$primary_value = isset($para['primary_value']) ? $para['primary_value'] : $db_args['primary_field_value'];
			
			// Check if any value exists with similar value
			// This will return total numbers matches found
			$total_matches = $db->check_value_exists( $db->set_table_prefix($table), array( $field => $value ), array( $primary_field => $primary_value ) );
			return !($total_matches > 0);
		}
		// Check if the field value is Number
		public function isNumber ($value) {
			return is_numeric($value);
		}
		// Check if the field value is Integer
		public function isInt ($value) {
			return filter_var($value, FILTER_VALIDATE_INT);
		}
		// Check if the field value is Float
		public function isFloat ($value) {
			return filter_var($value, FILTER_VALIDATE_FLOAT);
		}
		// Check if the field value is Boolean
		public function isBool ($value) {
			return filter_var($value, FILTER_VALIDATE_BOOLEAN);
		}
		// Check if the field value is URL
		public function isUrl ($value) {
			return filter_var($value, FILTER_VALIDATE_URL);
		}
		// Check if the field value is Domain
		public function isDomain ($value) {
			return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $value) //valid chars check
				&& preg_match("/^.{1,253}$/", $value) //overall length check
				&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $value)   ); //length of each label
		}
		// Check if the field value is Email
		public function isEmail ($value) {
			if ( !is_array($value) ) { $value = array($value); }
			foreach ( $value as $email ) {
				if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
					return false;
				}
			}
			return true;
		}
		// Check if the field value is JSON String
		public function isJson ($value) {
			return true;
		}
		// Check if the field value is IPv4
		public function isIpv4 ($value) {
			return filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
		}
		// Check if the field value is IPv6
		public function isIpv6 ($value) {
			return filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
		}
		/**
		 * Check if the field value is Date
		 * $date = Date or DateTime value
		 * $para = Includes an array of parrameters e.g. datetime-format, require=true
		 * $time = false: validae date, true: validate datetime
		 */
		public function isDateTime ($date, $para='', $time=false) {
			// Get date format from $para or default_date_format
			if ( $time === true ) {
				$formats = ( is_array($para) && isset( $para['format'] ) ) ? $para['format'] : $this->_args['default_datetime_format'];
			} else {
				$formats = ( is_array($para) && isset( $para['format'] ) ) ? $para['format'] : $this->_args['default_date_format'];
			}
			
			// If $formats is not array then convert it to array
			if ( !is_array($formats) ) { $formats = array($formats); }
			
			// Easy way to check date
			// Check if the date is valid for any of the formats in $formats array
			foreach ( $formats as $format ) {
				$d = DateTime::createFromFormat($format, $date);
				if ( $d && ($d->format($format) == $date) ) {
					// valid format
					return true;
				}
			}
			
			return false;
			/*
			$format = strtolower($format);
			
			 // find separator. Remove all other characters from $format 
			$separator_only = str_replace(array('m','d','y'),'', $format); 
			$separator = $separator_only[0]; // separator is first character 
			echo $format;

			// make regex 
            $regexp = str_replace('mm', '(0?[1-9]|1[0-2])', $format); 
            $regexp = str_replace('dd', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp); 
            $regexp = str_replace('yyyy', '(19|20)?[0-9][0-9]', $regexp); 
            $regexp = str_replace('yy', '[0-9][0-9]', $regexp); 
            $regexp = str_replace($separator, "\\" . $separator, $regexp); 
			echo $regexp;
            if($regexp != $date && preg_match('/'.$regexp.'\z/', $date)){ 
                // check date 
                $arr=explode($separator,$date); 
                $day=$arr[0]; 
                $month=$arr[1]; 
                $year=$arr[2]; 
                if(@checkdate($month, $day, $year)) {
                    return true; 
				}
            } 
			
			return false;
			
			$date = trim($date," ");

			// Replace slashes with dashes
			$date = str_replace($separator,"-",$date);
			*/
		}
		
		// Check if the field value is valid Password
		public function isValidPassword ( $value, $rule, $types = array() ) {
			if ( !is_array($types) ) { $types = array($types); }
			foreach ( $types as $type => $permission ) {
				// If permission is not true then skip validation
				if ( $permission !== true ) { continue; }
				switch ( strtolower($type) ) {
					case "digits"			:	$pattern = '/\d/'; break;
					case "capital_letters"	:	$pattern = '/[A-Z]/'; break;
					case "small_letters"	:	$pattern = '/[a-z]/'; break;
					default	: return true;
				}
				// Validate Pattern
				if ( !preg_match($pattern, $value) ) {			
					// Get error messages array
					$error[$rule] = $this->getErrMsg($rule, $this->_curErrMsgs, $type);
					$error = $this->applyStrReplace($error, array( 'val'=>$value, 'label'=>$this->_curElement['label']) );
					// Add the value to _errors[name] array
					$this->setCurrentElementErrMsg($error);
					return false;
				};
			}
			return true;
		}

		// Check if the field value Matches Pattern
		public function matchPattern ($value, $pattern) {
			if ( !is_array($value) ) { $value = array($value); }
			if ( !is_array($pattern) ) { $pattern = array($pattern); }
			foreach ( $value as $str ) {
				$total_match = 0;
				foreach ( $pattern as $patt ) {
					if ( !empty($patt) && preg_match($patt, $str) ) {
						$total_match++;
					}
				}
				if ( $total_match === 0 ) {
					return false;
				}
			}
			return true;
		}
		
		// Check if the field value Length not more than Maximum Length
		public function maxLength ($value, $length) {
			return is_array($value) ? (count($value)<=$length) : (strlen($value)<=$length);
		}
		// Check if the field value Length not less than Minimum Length
		public function minLength ($value, $length) {
			return is_array($value) ? (count($value)>=$length) : (strlen($value)>=$length);
		}
		// Check if the field value Length is equal to some Length
		public function equalLength ($value, $length) {
			if ( is_array($length) ) {
				// If value is array then compare total elements else compare string length
				return is_array($value) ? in_array(count($value), $length) : in_array(strlen($value), $length);
			} else {
				// If value is array then compare total elements else compare string length
				return is_array($value) ? (count($value)==$length) : (strlen($value)==$length);
			}
		}
		// Check if the field value is equal to some value
		public function isEqualTo ($value, $compae_value) {
			return ($value==$compae_value);
		}
		// Check if the field value not equal to some value
		public function isNotEqualTo ($value, $compae_value) {
			return ($value!=$compae_value);
		}
		// Check if the field value not more than Maximum Value
		public function isLessThan ($value, $compae_value) {
			return ($value<$compae_value);
		}
		// Check if the field value not more than Maximum Value
		public function isLessThanEqual ($value, $compae_value) {
			return ($value<=$compae_value);
		}
		// Check if the field value not less than Miniimum Value
		public function isGreaterThan ($value, $compae_value) {
			return ($value>$compae_value);
		}
		// Check if the field value not less than Miniimum Value
		public function isGreaterThanEqual ($value, $compae_value) {
			return ($value>=$compae_value);
		}
		// Check the field value contains any of the strings
		// Return false if the $value doesn't contain $string value
		public function mustContain ($value, $compare_str = array(), $case_sensitive = true ) {
			if ( !is_array($value) ) { $value = array($value); }
			if ( !is_array($compare_str) ) { $compare_str = array($compare_str); }
			$compare_func = (( $case_sensitive === true ) ? 'strpos' : 'stripos');
			foreach ( $value as $val ) {
				foreach ( $compare_str as $string ) {
					if ( $compare_func($val,$string) === false )  {
						return false;
					}
				}
			}
			return true;
		}
		// Check the field value must not contains any of the strings
		// Return false if the $value contains $string value
		public function mustNotContain ($value, $compare_str = array(), $case_sensitive = true ) {
			if ( !is_array($value) ) { $value = array($value); }
			if ( !is_array($compare_str) ) { $compare_str = array($compare_str); }
			$compare_func = (( $case_sensitive === true ) ? 'strpos' : 'stripos');
			foreach ( $value as $val ) {
				foreach ( $compare_str as $string ) {
					if ( $compare_func($val,$string) !== false )  {
						return false;
					}
				}
			}
			return true;
		}

		// Validate field with Custom Function
		public function validateCustomFunction ($value, $function_name, $args = false) {
			// Check if there is any value for custom function
			if ( !isset($function_name) || $function_name == '') {return true;}
			if ( !is_array($function_name) ) { $function_name = array($function_name); }
			foreach ( $function_name as $function ) {
				// Check if any function exists with the $para name
				if(method_exists($this,$function)) {
					// Execute function from curren class
					if ( !@call_user_func_array(array($this,$function), array($value,$args)) ) {
						return false;
					}
				} else if ( function_exists( $function ) ) {
					// Execute standalone function
					if ( !@call_user_func( $function, $value, $args ) ) {
						return false;
					}
				} else {
					return -1;
				}
			}
			return true;
		}
		
		/***********************************************************
		*
		*	Custom Functions
		*
		***********************************************************/
		
		/**
		 * Validate when one field depends on other field
		 * $value: value of current field
		 * $rule: rules to be validated on current field
		 */
		public function depends ( $value, $rules ) {
			$depend_on_field_value = ( isset($this->_data[$rules['depend_on']]) ? $this->_data[$rules['depend_on']] : false );
			// If Depends on field is valid then do validation
			if( $this->validateRule("require",$depend_on_field_value) === false ) {
				// If the depend on field is not valid then set validation for current field as true and do not validate
				return true;
			}
			
			// Remove 'depend_on' rule for validation
			unset($rules['depend_on']);
			
			return $this->processRules ($rules, $value, false, true);
		}
		/**
		 * Validate by Comparing with an Existing Field Value
		 * This function is run when one field is being compared with another field
		 * $value: Value of current field
		 * $rule: rules to be validated by comparing with another field
		 */
		public function compare ( $value, $rules ) {
			$compare_field = $rules['compare_to'];
			$compare_field_value = ( isset($this->_data[$compare_field]) ? $this->_data[$compare_field] : false );
			
			// Remove 'compare_to' rule for validation
			unset($rules['compare_to']);
			$isValid = true;
			$temp_rules = array();
			// Create rules by assigning $compare_field_value to compare keys
			foreach ( $rules as $rule => $permission ) {
				$temp_rules[$rule] = ( (strtolower($rule) == 'custom_validate') ? $permission : $compare_field_value );
			}
			return $this->processRules ($temp_rules, $value, false, true, array('compare_to'=>$compare_field,'compare_value'=>$compare_field_value));
		}
		
		/***********************************************************
		*
		*	Return Values in different Formats
		*
		***********************************************************/
		
		// Return Errors in JSON Format
		public function returnJsonResult ($pretty_print = false) {
			if ( $pretty_print === true ) {
				return json_encode($this->_errors, JSON_PRETTY_PRINT);
			} else {
				return json_encode($this->_errors);
			}
		}
		// Return Errors as PHP Array
		public function getErrors () {
			return $this->_errors;
		}
		// Return Errors Fields
		public function getErrorFields () {
			return array_keys($this->_errors);
		}
		// Return Error as PHP Array for a field
		public function getFieldError ($field_name) {
			return isset($this->_errors[$field_name]) ? $this->_errors[$field_name] : array();
		}
		// Return Errors Count
		public function getErrorCount () {
			return count($this->_errors);
		}
		// Return if all fields are valid
		public function isValid () {
			return $this->valid;
		}
	}
?>