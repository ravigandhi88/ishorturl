<?php
	/*
	 * Use this file to process any create resouce method
	 *
	 * - Create Link
	 */

	require_once("post_data.class.php");

	$response_debug['page'] = __FILE__;

	$shortner = new post_data();
	$shortner->data( $rest->get_request_data() );
	$shortner->post();
	$response['errors'] = $shortner->getErrors();
	$response['valid'] = $shortner->isValid;
	$response_debug['new_data'] = $shortner->data();
?>