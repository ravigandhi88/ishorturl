<?php
	/*

		Pending:
		- insert

	*/

	class post_data {

		protected $data = null;

		protected $errors = null;

		public $isValid = null;

		/*
		 * Set data and get data
		 */
		public function data ( $data = null ) {

			if ( $data ) $this->data = $data;

			return $this->data;

		}

		public function post () {

			// Get the ACTION from QueryString
			$action = strtolower($this->data[config::get("action_query_string")]);

			// Check if the ACTION is valid
			if ( !in_array( $action, config::get("query_string")) ) {
				$this->errors = array("action" => "Invalid action.");
				return false;
			}

			// Cleanup data
			$this->data_cleanup( $action );
			
			// Validate data
			$this->validate( $action );

			// if data is valid then Insert data to database
			if ( $this->isValid ) {
				$this->insert( $action );
			}

		}

		private function data_cleanup ( $action = null ) {

			if ( $action == "user" ) {

			} elseif ( $action == "domain" ) {

			} elseif ( $action == "link" ) {

				$this->data['domain'] = ( !isset($this->data['domain']) || !$this->data['domain'] ) ? DEFAULT_DOMAIN : $this->data['domain'];
				$this->data['domain_id'] = app::get_domain_id( $this->data['domain'] );
				$this->data['url'] = ( !isset($this->data['url']) || !$this->data['url'] ) ? '' : $this->data['url'];
				$this->data['title'] = ( !isset($this->data['title']) || !$this->data['title'] ) ? $this->fetch_page_title( $this->data['url'] ) : $this->data['title'];
					if ($this->data['title']) $this->data['title'] = substr( $this->data['title'], 0 , config::get("max_length")['title'] );
				$this->data['note'] = ( !isset($this->data['note']) || !$this->data['note'] ) ? '' : $this->data['note'];
					if ($this->data['note']) $this->data['note'] = substr( $this->data['note'], 0 , config::get("max_length")['note'] );
				$this->data['private'] = ( !isset($this->data['private']) ) ? 0 : 1;
				$this->data['short_link'] = app::create_short_link( $this->data['url'], $this->data['domain'] );

			} elseif ( $action == "visit" ) {

			} elseif ( $action == "log" ) {

			}

		}

		private function fetch_page_title ( $url ) {
			$title = app::get_page_title( $url );
			return $title ? $title : $url;
		}

		public function validate ( $action ) {

			if ( $action == "user" ) {

			} elseif ( $action == "domain" ) {

			} elseif ( $action == "link" ) {

				$_fields = array(
						array(
							'name' => 'domain',
							'label' => 'Domain',
							'input_type' => 'domain',
							'value' => $this->data['domain'],
							'validate' => array(
								'trim' => true,
								'rules' => array(
									'domain' => true,
									'max_length' => config::get("max_length")['domain']
								)
							),
						),
						array(
							'name' => 'url',
							'label' => 'URL',
							'input_type' => 'url',
							'value' => $this->data['url'],
							'validate' => array(
								'trim' => true,
								'rules' => array(
									'require' => true,
									'url' => true,
									'max_length' => config::get("max_length")['url']
								)
							),
						),
						array(
							'name' => 'title',
							'label' => 'Title',
							'input_type' => 'text',
							'value' => $this->data['domain'],
							'validate' => array(
								'trim' => true,
								'rules' => array(
									'max_length' => config::get("max_length")['title']
								)
							),
						),
						array(
							'name' => 'note',
							'label' => 'Note',
							'input_type' => 'text',
							'value' => $this->data['note'],
							'validate' => array(
								'trim' => true,
								'rules' => array(
									'max_length' => config::get("max_length")['note']
								)
							),
						),
						array(
							'name' => 'private',
							'label' => 'Private',
							'input_type' => 'text',
							'value' => @$this->data['private'],
							'validate' => array(
								'trim' => true,
								'rules' => array(
								)
							),
						)
					);

			} elseif ( $action == "visit" ) {

			} elseif ( $action == "log" ) {

			} 

			$validate = new validate(
				array(
					'_fields' => $_fields,
					'_data' => $this->data
				)
			);
			$validate->doMassValidation();
			$this->isValid = $validate->isValid();

			if ( !$this->isValid ) {
			
				$this->errors = array();

				foreach ($validate->getErrorFields() as $field ) {
					$this->errors[$field] = array_values($validate->getFieldError($field))[0];
				}

			}

			return $this->isValid;
		}

		private function insert ( $action ) {

			if ( $action == "user" ) {

				/*
				- id
				- domain_name
				- domain_ip
				- owner_id
				- default_url
				- public/private
				- creation_timestamp
				- update_timestamp
				*/

				$insert_data = array(
						'domain_name' => "",
						'domain_ip' => "",
						'owner_id' => "",
						'default_url' => "",
						'private' => "",
						'creation_timestamp' => "",
						'update_timestamp' => ""
					);

			} elseif ( $action == "domain" ) {

				/*
				- id
				- domain_name
				- domain_ip
				- owner_id
				- default_url
				- public/private
				- creation_timestamp
				- update_timestamp
				*/

				$insert_data = array(
						'domain_name' => "",
						'domain_ip' => "",
						'owner_id' => "",
						'default_url' => "",
						'private' => "",
						'creation_timestamp' => "",
						'update_timestamp' => ""
					);

			} elseif ( $action == "link" ) {

				/*
				- id
				- domain_id
				- owner_id
				- short_link
				- destination_url
				- destination_title
				- note
				- public/private
				- creation_timestamp
				- update_timestamp
				*/

				$insert_data = array(
						'domain_id' => $this->data['domain_id'],
						'short_link' => $this->data['short_link'],
						'destination_url' => $this->data['url'],
						'destination_title' => $this->data['title'],
						'note' => $this->data['note'],
						'private' => $this->data['private'],
						'owner_id' => '',
						'creation_timestamp' => '',
						'update_timestamp' => ''
					);

			} elseif ( $action == "visit" ) {

				/*
				- id
				- link_id
				- user_id (default: 0)
				- user_ip
				- user_country
				- referrer_domain
				- referrer_url
				- referrer_ip
				- referrer_country
				- timestamp
				*/

				$insert_data = array(
						'link_id' => "",
						'referrer_domain' => "",
						'referrer_url' => "",
						'referrer_ip' => "",
						'referrer_country' => "",
						'user_id' => "",
						'user_ip' => "",
						'user_country' => "",
						'timestamp' => ""
					);

			} elseif ( $action == "log" ) {

				/*
				- id
				- user_id
				- ip
				- country
				- details (e.g. login / login failed)
				- timestamp
				*/

				$insert_data = array(
						'user_id' => "",
						'ip' => "",
						'country' => "",
						'details' => "",
						'timestamp' => ""
					);

			}

		}

		public function getErrors () {
			return $this->errors;
		}

	}
?>