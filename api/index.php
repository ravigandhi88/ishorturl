<?php

	/*
		$response: Respnse sent to all users
		$response_debug: Response used only for debugging purpose
	*/

?><?php

	error_reporting(E_ALL);

	require_once("includes/class/restful_api.class.php");
	require_once("includes/class/db.class.php");
	require_once("includes/class/gf.class.php");
	require_once("includes/class/app.class.php");
	require_once("includes/class/validate.class.php");
	require_once("includes/class/url_shortner.class.php");
	require_once("includes/class/array2xml.class.php");

	require_once("includes/constants.php");
	require_once("includes/variables.php");
	require_once("includes/config.php");
	require_once("includes/connection.php");

?><?php

	$rest = new restful_api;
	$rest->method();
	$rest->content_type( config::get('response_content_type'));

	$response = array();

	$response_debug = array();
	$response_debug['config'] = config::get();
	$response_debug['user_request'] = $rest->get_request_data();
	$response_debug['method'] = $rest->method();
	$response_debug['mime-type'] = $rest->get_mime_type();

	// check if the mothod is valid method
	// if no: throw error
	// if yes: include necessary page and proceed
	if ( !$rest->is_valid_method() ) {

		$http_response_code = 405;

	} else {

		$http_response_code = 200;

		include( "methods/" . strtolower( $rest->method() ) . ".php");

	}

	// Set content type and http response code
	$rest->http_response_code( $http_response_code );
	
	// add values to response array
	$response['status_code'] = $rest->http_response_code();
	$response['status_txt'] = $http_status_codes[ $rest->http_response_code() ];

	// If debug is true add response_debug array in response
	if ( DEBUG === true ) $response['response_debug'] = $response_debug;

	// print on page
	echo $rest->return_response( $response );

?>